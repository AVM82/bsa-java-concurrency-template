package bsa.java.concurrency.config;

import bsa.java.concurrency.image.storage.InMemoryStorage;
import bsa.java.concurrency.image.storage.Storage;
import bsa.java.concurrency.utils.DHasher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class AppConfig {

    @Bean
    public DHasher dHasher() {
        return new DHasher();
    }

    @Bean
    public ExecutorService ExecutorPool() {
        return Executors.newFixedThreadPool(5);
    }

    @Bean
    public Storage inMemoryCache() {
        return new InMemoryStorage();
    }
}
