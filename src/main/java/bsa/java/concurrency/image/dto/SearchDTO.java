package bsa.java.concurrency.image.dto;


import bsa.java.concurrency.image.Image;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
public class SearchDTO implements SearchResultDTO {
    private final UUID imageId;
    private Double matchPercent;
    private final String imageUrl;

    @Override
    public void setMatchPercent(Double matchPercent) {
        this.matchPercent = matchPercent;
    }

    public static SearchDTO fromImage(Image image) {
        return SearchDTO
                .builder()
                .imageId(image.getId())
                .imageUrl(image.getUrl())
                .build();
    }
}
