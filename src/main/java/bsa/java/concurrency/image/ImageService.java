package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileStorageService;
import bsa.java.concurrency.image.dto.SearchDTO;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.storage.Storage;
import bsa.java.concurrency.utils.DHasher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ImageService {

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    Storage storage;

    @Autowired
    DHasher dHasher;

    private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    public String batch(MultipartFile[] files) throws IOException {
        return fileStorageService.processImage(files);
    }

    public String uploadFile(MultipartFile file) throws IOException {
        return fileStorageService.uploadFile(file);

    }

    public List<SearchResultDTO> searchFile(MultipartFile file, double threshold) throws IOException {

        List<SearchResultDTO> searchDTOS = new ArrayList<>();
        long dHash = dHasher.getHash(file.getBytes());
        for (Image image : storage.get()) {
            double mach = dHasher.getSimilarity(dHash, image.getDHash());
            if (mach >= threshold) {
                logger.info("Caught you! :)");
                searchDTOS.add(getSearchResultDTO(image, mach));
            }
        }
        uploadFile(file);
        return searchDTOS;
    }

    private SearchResultDTO getSearchResultDTO(Image image, double mach) {
        SearchResultDTO searchDTO = SearchDTO.fromImage(image);
        searchDTO.setMatchPercent(mach);
        return searchDTO;
    }

    public void deleteImage(UUID imageId) {
        fileStorageService.delete(storage.deleteById(imageId));
    }

    public void deleteAll() {
        storage.deleteAll();
        fileStorageService.deleteAll();
    }
}
