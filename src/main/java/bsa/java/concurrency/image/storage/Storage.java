package bsa.java.concurrency.image.storage;

import bsa.java.concurrency.image.Image;

import java.util.List;
import java.util.UUID;

public interface Storage {

    Void add(Long hash, String path);

    List<Image> get();

    boolean findByDHash(long dHash);

    Image deleteById(UUID imageId);

    void deleteAll();

}
