package bsa.java.concurrency.image.storage;

import bsa.java.concurrency.image.Image;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class InMemoryStorage implements Storage {

    private List<Image> inMemoryCache = new ArrayList<>();


    @Override
    public Void add(Long hash, String path) {
        if (!findByDHash(hash)) {
            UUID id = UUID.nameUUIDFromBytes(path.getBytes());
            inMemoryCache.add(new Image(id, hash, path));
        }
        return null;
    }

    @Override
    public List<Image> get() {
        return inMemoryCache;
    }

    @Override
    public boolean findByDHash(long dHash) {
        for (Image image : inMemoryCache) {
            if (image.getDHash() == dHash) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Image deleteById(UUID imageId) {
        for (int i = 0; i < inMemoryCache.size(); i++) {
            Image image = inMemoryCache.get(i);
            if (image.getId().equals(imageId)) {
                return inMemoryCache.remove(i);
            }
        }
        return null;
    }

    @Override
    public void deleteAll() {
        inMemoryCache = new ArrayList<>();
    }

}
