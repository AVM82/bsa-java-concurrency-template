package bsa.java.concurrency.image;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class Image {
    private final UUID id;
    private final long dHash;
    private final String url;

}
