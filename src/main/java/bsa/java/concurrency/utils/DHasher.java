package bsa.java.concurrency.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class DHasher {

    public final double MINIMUM_SIMILARITY_THRESHOLD = 0.85;
    private static final int IMAGE_SIZE = 9;
    private static final int MATRIX_SIZE = IMAGE_SIZE * IMAGE_SIZE;

    private double getDistance(long hash1, long hash2) {
        return countSetBits(hash1 ^ hash2);
    }

    private double countSetBits(long value) {
        double count = 0;
        for (int i = 0; i < 64; i++) {
            count += value & 1;
            value >>= 1;
        }
        return count;
    }

    public double getSimilarity(long hash1, long hash2) {
        double distance = getDistance(hash1, hash2);
        return 1.0 - (distance / MATRIX_SIZE);
    }

    private BufferedImage getBufferedImage(String path) throws IOException {
        return ImageIO.read(new File(path));
    }

    private BufferedImage getBufferedImage(byte[] image) throws IOException {
        return ImageIO.read(new ByteArrayInputStream(image));
    }

    public long getHash(String path) throws IOException {
        BufferedImage image = this.getBufferedImage(path);
        return this.getHash(image);
    }

    public long getHash(byte[] img) {
        BufferedImage image = null;
        try {
            image = this.getBufferedImage(img);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.getHash(image);
    }

    public long getHash(BufferedImage image) {
        // Resize original image
        var resizedImage = this.resize(image);

        //black and white
        var bwResizedImage = getBwResizedImage();
        bwResizedImage.getGraphics().drawImage(resizedImage, 0, 0, null);

        return calculateDHash(bwResizedImage);
    }

    private long calculateDHash(BufferedImage bwResizedImage) {
        long hash = 0;
        for (int row = 1; row < IMAGE_SIZE; row++) {
            for (int col = 1; col < IMAGE_SIZE; col++) {
                var prev = brightnessScore(bwResizedImage.getRGB(col - 1, row - 1));
                var current = brightnessScore(bwResizedImage.getRGB(col, row));
                hash |= current > prev ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }

    private int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

    private BufferedImage getBwResizedImage() {
        return new BufferedImage(IMAGE_SIZE, IMAGE_SIZE, BufferedImage.TYPE_BYTE_GRAY);
    }

    private Image resize(BufferedImage image) {
        return image.getScaledInstance(IMAGE_SIZE, IMAGE_SIZE, Image.SCALE_SMOOTH);
    }

    public boolean areSimilarHashes(long hash1, long hash2) {
        return this.getSimilarity(hash1, hash2) >= MINIMUM_SIMILARITY_THRESHOLD;
    }

    public boolean areSimilarHashes(long hash1, long hash2, double minimumSimilarityThreshold) {
        return this.getSimilarity(hash1, hash2) >= minimumSimilarityThreshold;
    }
}
