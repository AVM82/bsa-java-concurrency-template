package bsa.java.concurrency.fs;

import bsa.java.concurrency.config.StorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

import static org.apache.catalina.startup.ExpandWar.deleteDir;

@Repository
public class FileStorage implements FileSystem {

    @Autowired
    StorageProperties storageProperties;

    private static final Logger logger = LoggerFactory.getLogger(FileStorage.class);

    @Override
    public CompletableFuture<String> saveFile(MultipartFile file) {
        return CompletableFuture.supplyAsync(() -> {
            String path = storageProperties.getRootPath() + "//" + file.getOriginalFilename();
            File newFile = new File(path);
            try (FileOutputStream fos = new FileOutputStream(newFile);
                 InputStream inputStream = file.getInputStream()) {
                int b;
                while ((b = inputStream.read()) != -1) {
                    fos.write(b);
                }

            } catch (IOException e) {
                return e.getMessage();
            }
            return path;
        });
    }

    @Override
    public void deleteFile(String url) {
        deleteDir(new File(url));
    }

    @Override
    public void deleteAll() {
        String path = storageProperties.getRootPath();
        File rootDir = new File(path);
        File[] files = rootDir.listFiles();
        if (files != null) {
            for (final File file : files) {
                deleteDir(file);
            }
        }
    }

    private static void makeDir(File directory) {
        if (isDirectoryNotExist(directory)) {
            if (directory.mkdir()) {
                logger.info("Successfully created new directory : " + directory);
            } else {
                logger.info("Failed to create new directory: " + directory);
            }
        }
    }

    private static boolean isDirectoryNotExist(File directory) {
        if (directory.exists()) {
            logger.info("Directory already exists");
            return false;
        } else {
            logger.info("Directory not exists");
            return true;
        }
    }

    public static void makeWorkflow() {
        File directory = new File("upload");
        makeDir(directory);
    }
}
