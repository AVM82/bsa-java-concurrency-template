package bsa.java.concurrency.fs;

import bsa.java.concurrency.image.Image;
import bsa.java.concurrency.image.storage.Storage;
import bsa.java.concurrency.utils.DHasher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

@Service
public class FileStorageService {

    @Autowired
    FileStorage fileStorage;

    @Autowired
    DHasher dHasher;

    @Autowired
    ExecutorService executorService;

    @Autowired
    Storage storage;

    private static final Logger logger = LoggerFactory.getLogger(FileStorageService.class);

    public String uploadFile(MultipartFile file) throws IOException {
        if (file == null) {
            logger.error("Argument equals NULL");
            throw new IllegalArgumentException("Argument equals NULL");
        }
        byte[] bytes = file.getBytes();
        fileStorage.saveFile(file).thenAccept(
                (result) -> {
                    long dHash = dHasher.getHash(bytes);
                    storage.add(dHash, result);
                });

        return file.getOriginalFilename() + " was saved.";
    }

    public String processImage(MultipartFile[] files) throws IOException {
        if (files == null) throw new IllegalArgumentException("Argument equals NULL");

        List<CompletableFuture<Void>> futures = new ArrayList<>();
        for (MultipartFile file : files) {
            futures.add(getFutureProcessImage(file));
        }
        futures.forEach(CompletableFuture::join);
        return "DONE";
    }

    private CompletableFuture<Void> getFutureProcessImage(MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        return CompletableFuture.runAsync(() -> {
            CompletableFuture<String> save = fileStorage.saveFile(file);
            CompletableFuture<Long> dHashed = CompletableFuture.supplyAsync(() -> dHasher.getHash(bytes));
            CompletableFuture<Void> all = save.thenCombine(dHashed, (s, l) -> storage.add(l, s));
            all.join();

        }, executorService);
    }

    public void delete(Image image) {
        fileStorage.deleteFile(image.getUrl());
    }

    public void deleteAll() {
        fileStorage.deleteAll();
    }
}